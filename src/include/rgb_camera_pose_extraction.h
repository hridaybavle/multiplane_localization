#include <iostream>
#include <vector>
#include <sstream>
#include <utility>
#include <functional>
#include <numeric>
#include <boost/thread.hpp>
#include <string>
#include <cmath>

#include "opencv2/core.hpp"
#include "opencv2/opencv.hpp"
#include "opencv2/features2d.hpp"
#include "opencv2/flann.hpp"
#include "opencv2/imgcodecs.hpp"
#include "opencv2/highgui.hpp"
#include "opencv2/xfeatures2d.hpp"
#include "opencv2/xfeatures2d/nonfree.hpp"

class rgb_camera_pose_extraction
{

public:

    rgb_camera_pose_extraction();
    ~rgb_camera_pose_extraction();

public:

     void computeImageFeatures(cv::Mat image, std::vector<cv::KeyPoint> &keypoints, cv::Mat &descriptors, cv::Mat &output_image);
     void computeOnlyDescriptors(cv::Mat image, std::vector<cv::KeyPoint> keypoints, cv::Mat &descriptors);
     std::vector<cv::DMatch> computeFeatureMatches(cv::Mat left_image,cv::Mat descriptor_1, cv::Mat descriptor_2, std::vector<cv::KeyPoint> keypoints_1, std::vector<cv::KeyPoint> keypoints_2);
     cv::Mat computeFundamentalMatrix(std::vector<cv::Point2f> &points_1, std::vector<cv::Point2f> &points_2, std::vector<cv::DMatch> matches,
                                      std::vector<cv::KeyPoint> keypoints_1, std::vector<cv::KeyPoint> keypoints_2, std::vector<cv::DMatch>& out_matches);
     void computeEpipolarlines(cv::Mat image_1, cv::Mat image_2, std::vector<cv::Point2f> image_points_1, std::vector<cv::Point2f> image_points_2, cv::Mat fundamental_mat);
     cv::Mat computeEssentialMatrix(std::vector<cv::Point2f> points_1, std::vector<cv::Point2f> points_2, cv::Mat fundamental_matrix, cv::Mat K);
     void computeRotationandTranslation(std::vector<cv::Point2f> &points_1, std::vector<cv::Point2f> &points_2,
                                        cv::Mat K, cv::Mat essential_mat, cv::Mat &rotation_mat, cv::Mat &translation_vec);
     void computeLinearTriangulation(cv::Mat K, std::vector<cv::Point2f> points_1, std::vector<cv::Point2f> points_2, cv::Mat &points3D,
                                     cv::Mat R1, cv::Mat T1, cv::Mat R2, cv::Mat T2);

     void compute3DpointDescriptors(cv::Mat points_3d, cv::Mat descriptor3D);
     void correspondences2D3D(cv::Mat points3D, std::vector<cv::DMatch> third_matches,
                              std::vector<cv::KeyPoint> keypoints_2, std::vector<cv::KeyPoint> keypoints_3,
                              std::vector<cv::Point2f> points_2, std::vector<cv::Point2f>& new_points_3, cv::Mat& new_points3D);
     void sovlePnPRansac(std::vector<cv::Point2f> points_3, cv::Mat points3D, cv::Mat K, cv::Mat dist_coeff, cv::Mat& R3, cv::Mat& T3);
     void symmetryTest(const std::vector<cv::DMatch>& matches1, const std::vector<cv::DMatch>& matches2, std::vector<cv::DMatch>& sym_matches);

};
