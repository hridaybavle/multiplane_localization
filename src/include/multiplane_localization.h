
//I/O stream
//std::cout
#include <iostream>
#include <vector>
#include <sstream>
#include <utility>
#include <functional>
#include <numeric>
#include <boost/thread.hpp>
#include <string>
#include <cmath>

// Eigen
#include <eigen3/Eigen/Core>
#include <Eigen/Dense>

// Boost
#include <boost/foreach.hpp>


// OpenCV
#include "opencv2/imgproc/imgproc.hpp"
#include "opencv2/highgui/highgui.hpp"
#include "opencv2/features2d/features2d.hpp"
#include "opencv2/opencv.hpp"


// PCL



#include <pcl/ModelCoefficients.h>
#include <pcl/point_types.h>
#include <pcl/sample_consensus/method_types.h>
#include <pcl/sample_consensus/sac_model_plane.h>
#include <pcl/segmentation/sac_segmentation.h>
#include <pcl/filters/statistical_outlier_removal.h>
#include <pcl/filters/voxel_grid.h>
#include <pcl/filters/extract_indices.h>
#include <pcl/filters/filter_indices.h>
#include <pcl/filters/filter.h>
#include <pcl/filters/passthrough.h>
#include <pcl/io/io.h>
#include <pcl/io/pcd_io.h>
#include <pcl/registration/icp.h>
#include <pcl/keypoints/harris_3d.h>
#include <pcl/features/normal_3d.h>
#include <pcl/visualization/pcl_visualizer.h>
#include <pcl/features/fpfh.h>
#include <pcl/search/kdtree.h>
#include <pcl/registration/correspondence_rejection_sample_consensus.h>
#include <pcl/segmentation/region_growing.h>
#include <pcl/filters/filter.h>
#include <pcl/segmentation/organized_multi_plane_segmentation.h>
#include <pcl/features/integral_image_normal.h>
#include <pcl/io/pcd_io.h>
#include <vtkRenderWindow.h>
#include <pcl/segmentation/extract_clusters.h>
#include <pcl/features/normal_3d_omp.h>

// ROS
#include <ros/ros.h>
#include <dynamic_reconfigure/DoubleParameter.h>
#include <dynamic_reconfigure/Reconfigure.h>
#include <dynamic_reconfigure/Config.h>

// PCL ROS
#include <pcl_conversions/pcl_conversions.h>
#include <pcl_ros/point_cloud.h>
#include <pcl_msgs/ModelCoefficients.h>


//ROS Images
#include <image_transport/image_transport.h>
#include <cv_bridge/cv_bridge.h>
#include <sensor_msgs/Image.h>
#include <sensor_msgs/image_encodings.h>
#include <sensor_msgs/PointCloud2.h>
#include <sensor_msgs/Imu.h>
#include <geometry_msgs/TransformStamped.h>

// Camera calibration
#include <sensor_msgs/CameraInfo.h>

//tf
#include "tf/transform_datatypes.h"

//geometry msgs
#include "geometry_msgs/PointStamped.h"

//EKF_SLAM class
#include "EKF_SLAM.h"

//rgb pose calculation
#include "rgb_camera_pose_extraction.h"

#define NODE_FREQUENCY       60
#define CV_TERMCRIT_ITER     1
#define CV_TERMCRIT_NUMBER   CV_TERMCRIT_ITER
#define CV_TERMCRIT_EPS      2
#define NUM_CENTROIDS        3
#define NUM_HEIGHT_CENTROIDS 3
#define CAM_PITCH_ANGLE  50*(M_PI/180)
#define STATE_SIZE           1
#define NOISE_VECTOR         1
#define MEASUREMENT_SIZE     1
//#define debug_mode


class multiPlaneLocalization
{

protected:
    ekf_slam ekf_localization;
    rgb_camera_pose_extraction camera_pose_extraction_obj_;

public:
    multiPlaneLocalization(int argc, char **argv);
    ~multiPlaneLocalization();

public:
    bool run();
    void open(ros::NodeHandle n);
    bool ekf_run();

protected:
    ros::Subscriber point_cloud_subscriber_;
    void pointCloudCallback(const sensor_msgs::PointCloud2ConstPtr& msg);

    ros::Subscriber rgb_image_subscriber_;
    void rgbImageCallback(const sensor_msgs::Image & msg);

    ros::Subscriber rgb_cam_info_subscriber_;
    void rgbCamInfoCallback(const sensor_msgs::CameraInfo & msg);

    ros::Subscriber imu_angles_subscriber_;
    void imuAnglesCallback(const sensor_msgs::ImuConstPtr& msg);

    ros::Subscriber vicon_angles_subscriber_;
    void viconAnglesCallback(const geometry_msgs::TransformStamped& msg);

    ros::Publisher altitude_in_camera_pub_;
    inline void publishRefinedAltitude(float refined_altitude, ros::Time time_stamp);

     void readParams();
     std::string altitude_topic_name_;
     std::string point_cloud_topic_name_;
     std::string rgb_image_topic_name_;
     std::string rgb_cam_info_topic_name_;
     std::string imu_topic_name_;

     cv_bridge::CvImagePtr cvFrontImage_;
     cv::Mat rgb_image_3_, rgb_image_1_, rgb_image_2_;

     double camera_pitch_angle_, altitude_add_;
     bool use_dataset_kinect, use_dataset_euroc;
protected:
    double yaw_, pitch_, roll_;
    float pose_x_, pose_y_, pose_z_;
    cv::Mat K_mat_, dist_coeff_;

protected:
    pcl::PointCloud<pcl::PointXYZ>::Ptr cloud_msg_const_;
    pcl::PointCloud<pcl::PointXYZ> cloud_msg_;

    //2D Image
    sensor_msgs::Image image_;
    cv::Mat  mat_image_;

    //global variables
    float prev_ground_plane_, prev_plane_2_;
    std::vector<float> interplane_distance_, prev_preli_inter_plane_dist_;
    std::vector<float> abscent_inter_plane_dist_;
    std::vector<float> abs_max_plane_;
    std::vector<float> prev_map_;
    std::vector<bool>   max_plane_absent_;
    bool second_plane_present_;
    int second_plane_counter_, image_counter_;
    int counter_;
    float altitude_;
    bool new_odom_measurements_ , new_map_measurements_, publish_altitude_, module_started_;

    //pcl viewer
    boost::shared_ptr<pcl::visualization::PCLVisualizer> kmeansSegViewer;

    std_msgs::Header header_;

protected:

    void setEkfModel(float start_altitude, float maha_threshold);
    pcl::PointCloud<pcl::Normal>::Ptr normalSegmentation(pcl::PointCloud<pcl::PointXYZ>::Ptr pointT);
    float computeGroundPlane(pcl::PointCloud<pcl::PointXYZ>::Ptr pointT, pcl::PointCloud<pcl::Normal>::Ptr normals);
    void computeKmeansGroundPlane(pcl::PointCloud<pcl::PointXYZ>::Ptr pointT, pcl::PointCloud<pcl::Normal>::Ptr normals);
    void transformCameraToWorldFrame(Eigen::Matrix4f &transformation_matrix);
    pcl::PointCloud<pcl::PointXYZ>::Ptr organisedDownSampling(pcl::PointCloud<pcl::PointXYZ>::Ptr pointT);

};
