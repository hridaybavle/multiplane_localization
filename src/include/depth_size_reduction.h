
//I/O stream
//std::cout
#include <iostream>
#include <vector>
#include <sstream>
#include <utility>
#include <functional>
#include <numeric>
#include <boost/thread.hpp>

#include <cmath>

// Eigen
#include <eigen3/Eigen/Core>
#include <Eigen/Dense>

// Boost
#include <boost/foreach.hpp>

//PCL
#include <pcl/point_cloud.h>
#include <pcl/common/time.h>

// PCL ROS
#include <pcl_conversions/pcl_conversions.h>
#include <pcl_ros/point_cloud.h>
#include <pcl_msgs/ModelCoefficients.h>


// OpenCV
#include "opencv2/imgproc/imgproc.hpp"
#include "opencv2/highgui/highgui.hpp"
#include "opencv2/features2d/features2d.hpp"
#include "opencv2/opencv.hpp"


// ROS
#include <ros/ros.h>

//ROS Images
#include <image_transport/image_transport.h>
#include <cv_bridge/cv_bridge.h>
#include <sensor_msgs/Image.h>
#include <sensor_msgs/image_encodings.h>
#include <sensor_msgs/PointCloud2.h>
#include <sensor_msgs/Imu.h>

// Camera calibration
#include <sensor_msgs/CameraInfo.h>


//#define debug_mode




class depthSizeReduction
{
public:
    depthSizeReduction(int argc,char **argv);
    ~depthSizeReduction();

public:
    bool run();
    void open(ros::NodeHandle n);

 protected:
    ros::Subscriber depth_image_subscriber_;
    void depthImagecallback(const sensor_msgs::Image& msg);

    ros::Subscriber depth_cam_info_subscriber_;
    void depthCamInfoCallback(const sensor_msgs::CameraInfo& msg);

    ros::Subscriber ir_cam_right_image_subsriber_;
    void irRightCamImageCallback(const sensor_msgs::Image& msg);

    ros::Subscriber ir_cam_left_image_subsriber_;
    void irLeftCamImageCallback(const sensor_msgs::Image& msg);

    ros::Subscriber cam_left_image_subsriber_;
    void LeftCamImageCallback(const sensor_msgs::Image& msg);

    ros::Subscriber cam_right_image_subsriber_;
    void RightCamImageCallback(const sensor_msgs::Image& msg);


    image_transport::Publisher downsampled_depth_image_pub_;
    image_transport::Publisher downsampled_ir_right_image_pub_;
    image_transport::Publisher downsampled_ir_left_image_pub_;
    ros::Publisher depth_cam_info_pub_;
    ros::Publisher downsampled_point_cloud_;

    image_transport::Publisher left_cam_image_pub_;
    image_transport::Publisher right_cam_image_pub_;

    ros::Publisher left_cam_info_pub_;
    ros::Publisher right_cam_info_pub_;


};
