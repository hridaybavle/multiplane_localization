#include "multiplane_localization.h"

using namespace std;

multiPlaneLocalization::multiPlaneLocalization(int argc,char **argv):
    cloud_msg_const_ (new pcl::PointCloud<pcl::PointXYZ>)
  //meansSegViewer (new pcl::visualization::PCLVisualizer ("kmeans normal segmentation"))
{
    roll_ = 0, pitch_ = 0; yaw_ = 0;
    pose_x_ = 0, pose_y_ = 0, pose_z_ = 0;
    interplane_distance_.clear(), prev_ground_plane_ = 0, prev_plane_2_ =0;
    max_plane_absent_.clear(); abs_max_plane_.clear(), prev_preli_inter_plane_dist_.clear();
    abscent_inter_plane_dist_.clear();
    counter_ =0, prev_map_.clear(), second_plane_present_= false;
    second_plane_counter_=0, image_counter_ = 0;
    altitude_ =0;
    new_odom_measurements_ = false, new_map_measurements_ = false;
    publish_altitude_ = false, module_started_ = false;
    K_mat_ = cv::Mat::zeros(3,3, CV_64F);
    dist_coeff_ = cv::Mat::zeros(5,1, CV_64F);

    cout << "opening multiPlaneLocalization node" << endl;
    return;
}


multiPlaneLocalization::~multiPlaneLocalization()
{

    cout << "destroying the multiPlaneLocalization node" << endl;
    return;

}

inline bool comparator(const float& lhs, const float& rhs)
{
    return lhs > rhs;
}

void multiPlaneLocalization::setEkfModel(float start_altitude, float maha_threshold)
{
    Eigen::VectorXd state(STATE_SIZE);
    Eigen::MatrixXd cov_kalman(STATE_SIZE,STATE_SIZE), jacob_state(STATE_SIZE, STATE_SIZE);
    Eigen::MatrixXd jacob_noise(NOISE_VECTOR, STATE_SIZE), cov_model_noise(STATE_SIZE, STATE_SIZE);

    Eigen::MatrixXd jacob_state_meas(MEASUREMENT_SIZE, STATE_SIZE) , jacob_meas_meas(MEASUREMENT_SIZE, MEASUREMENT_SIZE);
    Eigen::MatrixXd cov_innovation(MEASUREMENT_SIZE, MEASUREMENT_SIZE), cov_meas_noise(MEASUREMENT_SIZE, MEASUREMENT_SIZE);
    Eigen::VectorXd z_est (MEASUREMENT_SIZE), innovation(MEASUREMENT_SIZE);
    Eigen::MatrixXd kalman_gain (STATE_SIZE, MEASUREMENT_SIZE);

    state.setZero(), cov_kalman.setZero(), jacob_noise.setZero();
    jacob_state.setZero(), cov_model_noise.setZero();

    jacob_state_meas.setZero(), jacob_meas_meas.setZero();
    cov_innovation.setZero(), cov_meas_noise.setZero();
    z_est.setZero(), innovation.setZero(), kalman_gain.setZero();

    //process model variables
    state(0)             = start_altitude;      // state tz
    cov_kalman(0,0)      = 0.001; // Covariance matrix
    jacob_state(0,0)     = 1;    // Jacobians with respect to the state
    jacob_noise(0,0)     = 1;    // Jacobians with respect to the noise
    cov_model_noise(0,0) = 0.001; // process noise covariance matrix

    //measurement model variables
    jacob_state_meas(0) = 1;     // jacobians of state with respect to measurement
    jacob_meas_meas(0)  =-1;    // jacobians of measurement with respect ot the measurement
    cov_meas_noise(0)   = 0.001;  // measurement covriance R

    //setting the ekf maha_threshold for matching
    ekf_localization.setMahaDistance(maha_threshold);

    //filling the state and measurement size variables
    ekf_localization.setStateandMeasurementSize(STATE_SIZE, MEASUREMENT_SIZE);

    //setting the ekf process model
    ekf_localization.setProcessModel(state, cov_kalman, jacob_state, jacob_noise, cov_model_noise);

    //setting the ekf measurement model
    ekf_localization.setMeasurementModel(z_est, innovation, jacob_state_meas, jacob_meas_meas,
                                         cov_innovation, cov_meas_noise, kalman_gain);


}

bool multiPlaneLocalization::run()
{
    //Display Window 2
    //    kmeansSegViewer->setBackgroundColor (0, 0, 0);
    //    kmeansSegViewer->initCameraParameters ();
    //    kmeansSegViewer->setCameraPosition(0,0,0,0,0,1,0,-1,0);
    //    vtkSmartPointer<vtkRenderWindow> renderWindowNormals = kmeansSegViewer->getRenderWindow();
    //    renderWindowNormals->SetSize(800,450);
    //    renderWindowNormals->Render();

    ros::Rate rate(NODE_FREQUENCY);
    while(ros::ok())
    {
        //while(!kmeansSegViewer->wasStopped())
        {
            ros::spinOnce();

            if (module_started_)
            {
                //Run the EKF_SLAM
                if(!ekf_run())
                    cout << "error in running the ekf " << endl;


                //This is just for testing, subtracting 12cm from the altitude before pub, as this is the transformation between the lidar and the rgb
                //TODO: This should change in the EKF, you should add 12cm to the lidar data, as the rgb is in the robot center.
                if(altitude_>0 && publish_altitude_ && !use_dataset_kinect)
                    publishRefinedAltitude(altitude_+altitude_add_, header_.stamp);
                else if (altitude_ ==0 && publish_altitude_ && !use_dataset_kinect)
                    publishRefinedAltitude(altitude_, header_.stamp);
                else if (use_dataset_kinect)
                    publishRefinedAltitude(-altitude_+altitude_add_, header_.stamp);

            }
            //kmeansSegViewer->spinOnce(100);
            rate.sleep();
        }
    }

}

bool multiPlaneLocalization::ekf_run()
{
    //if (new_odom_measurements_ == true)
    //run the ekf_slam prediction stage everytime
    altitude_ = ekf_localization.prediction();
    //new_odom_measurements_ = false;


    if (new_map_measurements_ ==true)
    {
        //run the ekf update stage when the map data is present
        altitude_ = ekf_localization.update();
        new_map_measurements_ = false;
    }

    publish_altitude_ = true;
    return true;
}

void multiPlaneLocalization::computeKmeansGroundPlane(pcl::PointCloud<pcl::PointXYZ>::Ptr pointT, pcl::PointCloud<pcl::Normal>::Ptr normals)
{

    double algo_start = pcl::getTime();

    pcl::PointCloud<pcl::PointXYZ>::Ptr points_without_nans (new pcl::PointCloud<pcl::PointXYZ>);
    pcl::PointCloud<pcl::PointXYZ>::Ptr kmeans_segmented_points (new pcl::PointCloud<pcl::PointXYZ>);

    Eigen::Matrix4f transformation_matrix;
    transformation_matrix.setZero(4,4);

    double kmeans_ground_plane_seg_start = pcl::getTime();

    transformCameraToWorldFrame(transformation_matrix);
    //cout << "transformation_matrix " << transformation_matrix << endl;

    Eigen::Vector4f normal_coefficients, normal_coefficients_transformed, points, transformed_points, normal_coefficients_transformed_world,
            normal_coefficients_ground_plane;

    normal_coefficients.setZero(), normal_coefficients_transformed.setZero(), points.setZero(), transformed_points.setZero(),
            normal_coefficients_transformed_world.setZero(), normal_coefficients_ground_plane.setZero();


    normal_coefficients_transformed_world(0) = 0;
    normal_coefficients_transformed_world(1) = 0;
    normal_coefficients_transformed_world(2) = 1;

    normal_coefficients_ground_plane = (transformation_matrix.transpose().eval()) * normal_coefficients_transformed_world;
#ifdef debug_mode
    cout << "normal coefficients for ground planes" << normal_coefficients_ground_plane << endl;
#endif

    // Kmeans algorithm
    int normal_counter = 0;
    cv::Mat normal_points, normal_filtered_points;


    normal_points = cv::Mat(normals->size(),3, CV_32F);
    normal_filtered_points = cv::Mat::zeros(0,3, CV_32F);

    for (size_t i =0; i < normals->size(); ++i)
    {
        if(!std::isnan(normals->points[i].normal_x) && !std::isnan(normals->points[i].normal_y)
                && !std::isnan(normals->points[i].normal_z))
        {
            normal_points.at<float>(i,0) =   static_cast<float>(normals->points[i].normal_x);
            normal_points.at<float>(i,1) =   static_cast<float>(normals->points[i].normal_y);
            normal_points.at<float>(i,2) =   static_cast<float>(normals->points[i].normal_z);

            //normal_filtered_points.push_back(normal_points.row(i));

            points_without_nans->points.push_back(pointT->points[i]);
            normal_filtered_points.push_back(normal_points.row(i));
            //            normal_counter++;
        }

    }

    if (normal_filtered_points.rows <= 10)
    {
        //cout << "\033[1;31m Not a lot of points for Kmeans \033[0m\n " << endl;
        return;
    }
    //cout << "normal filtered points size " << normal_filtered_points.rows << endl;
    //cout << "normal points " << normal_points << endl;
    //    cout << "normal counter " << normal_counter << endl;
    //    cout << "normal filtered points size " << normal_filtered_points.size() << endl;
    //    cout << "points_without_nans size " << points_without_nans->size() << endl;


    cv::Mat centroids, labels;
    double first_kmeans_seg_start = pcl::getTime();

    double kmeans_start_time = pcl::getTime();
    double compactness = 0.0;
    cv::TermCriteria criteria_kmeans(CV_TERMCRIT_EPS+CV_TERMCRIT_ITER, 3, 0.01);
    compactness = cv::kmeans(normal_filtered_points, NUM_CENTROIDS, labels, criteria_kmeans, 3, cv::KMEANS_RANDOM_CENTERS, centroids);
    double kmeans_stop_time = pcl::getTime();

    //cout << "labels "  << labels << endl;
#ifdef debug_mode
    cout << "kmeans time " << kmeans_stop_time - kmeans_start_time << endl;
    cout << "centroids " << centroids << endl;
#endif
    cv::Mat filtered_centroids = cv::Mat::zeros(0, 3, CV_32F);;

    int filtered_centroids_counter = -1;
    for(int i = 0; i < NUM_CENTROIDS; i++)
    {
        if(centroids.at<float>(i,0) < normal_coefficients_ground_plane(0)+0.3 && centroids.at<float>(i,0) > normal_coefficients_ground_plane(0)-0.3
                && centroids.at<float>(i,1) < normal_coefficients_ground_plane(1)+0.3 && centroids.at<float>(i,1) > normal_coefficients_ground_plane(1)-0.3
                && centroids.at<float>(i,2) < normal_coefficients_ground_plane(2)+0.3 && centroids.at<float>(i,2) > normal_coefficients_ground_plane(2)-0.3)
        {
            filtered_centroids.push_back(centroids.row(i));
            filtered_centroids_counter = i;
        }

    }
#ifdef debug_mode
    cout << "filtered centroid counters " << filtered_centroids_counter << endl;
#endif
    if(filtered_centroids.empty())
        return;
#ifdef debug_mode
    //cout << "filtered centroids rows " << filtered_centroids.rows << endl;
    cout << "filtered centroids " << filtered_centroids << endl;
#endif

    //segmenting the pointcloud using the labels from the 1st kmeans
    for(size_t i = 0; i < points_without_nans->size(); ++i)
    {
        if(labels.at<int>(i,0) == filtered_centroids_counter)
            kmeans_segmented_points->points.push_back(points_without_nans->points[i]);
    }

    double first_kmeans_seg_stop = pcl::getTime();
#ifdef debug_mode
    cout << "first kmeans seg took " << first_kmeans_seg_stop - first_kmeans_seg_start << endl;
#endif
    //    kmeansSegViewer->removeAllPointClouds();
    //    //pcl::visualization::PointCloudColorHandlerRGBField<pcl::PointXYZ> rgb(kmeans_segmented_points);
    //    kmeansSegViewer->addPointCloud<pcl::PointXYZ>(kmeans_segmented_points,"kmeans segmented cloud");
    //    kmeansSegViewer->setPointCloudRenderingProperties (pcl::visualization::PCL_VISUALIZER_POINT_SIZE, 1, "kmeans segmented cloud");

    double height_vec_com_start = pcl::getTime();

    std::vector<float> height_vector;
    float height;

    for (size_t i = 0; i < kmeans_segmented_points->size(); ++i)
    {
        height = kmeans_segmented_points->points[i].x * normal_coefficients_ground_plane(0) +
                kmeans_segmented_points->points[i].y * normal_coefficients_ground_plane(1) +
                kmeans_segmented_points->points[i].z * normal_coefficients_ground_plane(2);

        height = -1*height;
        height_vector.push_back(height);
    }


    //returning if the heigh vector size is less that 5 for avoiding failure of kmeans
    if(height_vector.size() <= 5)
        return;

    double height_vec_com_stop = pcl::getTime();
#ifdef debug_mode
    cout << "height_vec_com took" << height_vec_com_stop - height_vec_com_start << endl;
#endif
    //applying second kmeans for height rejection
    cv::Mat height_points;
    height_points = cv::Mat(height_vector.size(),1, CV_32F);

    for(size_t i =0; i < height_vector.size(); ++i)
    {
        height_points.at<float>(i,0) = (float) height_vector.at(i);
    }

    // cout << "height points " << height_points << endl;

    cv::Mat height_centroids, height_labels;
    double height_kmeans_start_time = pcl::getTime();
    double height_compactness = 0.0;
    cv::TermCriteria height_criteria_kmeans(CV_TERMCRIT_EPS+CV_TERMCRIT_ITER, 5, 0.01);
    height_compactness = cv::kmeans(height_points, NUM_HEIGHT_CENTROIDS, height_labels, height_criteria_kmeans, 5, cv::KMEANS_RANDOM_CENTERS, height_centroids);
    double height_kmeans_stop_time = pcl::getTime();
    //#ifdef debug_mode
    //    cout << "height kmeans time " << height_kmeans_stop_time - height_kmeans_start_time << endl;
    //    cout << "height centroids "   << height_centroids << endl;
    //#endif
    vector<float> planes;

    planes.clear();
    for(int i =0; i < NUM_HEIGHT_CENTROIDS; i++)
    {
        //if (height_centroids.at<float>(i,0) > 0.0)
        planes.push_back(height_centroids.at<float>(i,0));
    }

    //sorting the planes from low to highest number
    std::sort(planes.begin(), planes.end(), comparator);
    ekf_localization.clearPlanes();
    for(int i =0; i < planes.size(); ++i)
    {
        ekf_localization.setObservationData(i, planes[i]);
    }
    new_map_measurements_ = true;

    //    altitude_ = planes[0];
    //    publish_altitude_ = true;
#ifdef debug_mode
    for(int i=0; i<planes.size(); i++)
        cout << "planes sorted " << planes[i] << endl;
#endif


    double algo_stop = pcl::getTime();
    //    cout << "Total algo time " << algo_stop - algo_start << endl;

    return;

}

void multiPlaneLocalization::readParams()
{
    ros::param::get("~altiude_topic_name", altitude_topic_name_);
    if(altitude_topic_name_.length() ==0 )
        altitude_topic_name_ = "altitudeRGBD";
    cout << "altitude_topic_name " << altitude_topic_name_ << endl;


    ros::param::get("~point_cloud_topic_name", point_cloud_topic_name_);
    if(point_cloud_topic_name_.length() == 0)
        point_cloud_topic_name_ = "/camera/depth/points";
    cout << "point_cloud_topic_name " << point_cloud_topic_name_  << endl;

    ros::param::get("~rgb_image_topic_name", rgb_image_topic_name_);
    if(rgb_image_topic_name_.length() == 0)
        rgb_image_topic_name_ = "/camera/color/image_rect_color";
    cout << "rgb_image_topic_name " << rgb_image_topic_name_  << endl;


    ros::param::get("~rgb_cam_info_topic_name", rgb_cam_info_topic_name_);
    if(rgb_cam_info_topic_name_.length() == 0)
        rgb_cam_info_topic_name_ = "/camera/color/camera_info";
    cout << "rgb_image_topic_name " << rgb_image_topic_name_  << endl;

    ros::param::get("~imu_topic_name", imu_topic_name_);
    if(imu_topic_name_.length() ==0)
        imu_topic_name_ == "mavros/imu/data";
    std::cout << "imu_topic_name " << imu_topic_name_ << endl;

    ros::param::get("~camera_angle", camera_pitch_angle_);
    cout << "camera_pitch_angle " << camera_pitch_angle_ << endl;
    //converting camera_pitch_angle to radians
    camera_pitch_angle_ = camera_pitch_angle_*(M_PI/180);

    ros::param::get("~altitude_add", altitude_add_);
    cout << "altitude_add " << altitude_add_ << endl;

    ros::param::get("~use_dataset_kinect", use_dataset_kinect);
    cout << "use_dataset_kinect " << use_dataset_kinect << endl;

    ros::param::get("~use_dataset_euroc", use_dataset_euroc);
    cout << "use_dataset_euroc " << use_dataset_euroc << endl;

    std::string depth_compressed_set_params;
    ros::param::get("~depth_compressed_set_params", depth_compressed_set_params);
    if (depth_compressed_set_params.length() == 0)
        depth_compressed_set_params="/camera/depth/image_raw/compressed/set_parameters";
    cout << "depth_compressed_set_params " <<  depth_compressed_set_params << endl;

    //This sets the compression format for depth image to png instead of jpeg
    dynamic_reconfigure::ReconfigureRequest srv_req;
    dynamic_reconfigure::ReconfigureResponse srv_res;
    dynamic_reconfigure::StrParameter str_param;
    dynamic_reconfigure::Config conf;

    str_param.name = "format";
    str_param.value = "png";
    conf.strs.push_back(str_param);
    srv_req.config = conf;

    ros::service::call(depth_compressed_set_params, srv_req, srv_res);

    float start_altitude;
    ros::param::get("~start_altitude", start_altitude);
    if (start_altitude == 0)
        start_altitude = 0.0;

    cout << "start altitude " << start_altitude << endl;

    float maha_threshold;
    ros::param::get("~maha_threshold", maha_threshold);
    if (maha_threshold == 0)
        maha_threshold = 0.0;

    cout << "maha_threshold " << maha_threshold << endl;

    setEkfModel(start_altitude, maha_threshold);

}

void multiPlaneLocalization::open(ros::NodeHandle n)
{
    readParams();

    altitude_in_camera_pub_      = n.advertise<geometry_msgs::PoseStamped>(altitude_topic_name_, 1);

    point_cloud_subscriber_     = n.subscribe(point_cloud_topic_name_, 1 , &multiPlaneLocalization::pointCloudCallback, this);
    rgb_image_subscriber_       = n.subscribe(rgb_image_topic_name_, 1, &multiPlaneLocalization::rgbImageCallback, this);
    rgb_cam_info_subscriber_    = n.subscribe(rgb_cam_info_topic_name_,1, &multiPlaneLocalization::rgbCamInfoCallback, this);
    imu_angles_subscriber_      = n.subscribe(imu_topic_name_, 1, &multiPlaneLocalization::imuAnglesCallback, this);


    vicon_angles_subscriber_    = n.subscribe("/vicon/vehicle_20",1,&multiPlaneLocalization::viconAnglesCallback, this);

    return;

}


void multiPlaneLocalization::pointCloudCallback(const sensor_msgs::PointCloud2ConstPtr &msg)
{
#ifdef debug_mode
    cout << "Time Delay in acquisition " << (ros::Time::now() - msg->header.stamp).toSec() << endl;
#endif
    module_started_ = true;

    pcl::PointCloud<pcl::PointXYZ>::Ptr cloud_n_ (new pcl::PointCloud<pcl::PointXYZ>);
    pcl::PointCloud<pcl::Normal>::Ptr normal_cloud_  (new pcl::PointCloud<pcl::Normal>);

    header_.stamp = msg->header.stamp;

    pcl::fromROSMsg(*msg, *cloud_n_);

    // Create the filtering object
    //    pcl::VoxelGrid<pcl::PointXYZRGB> voxelGrid;
    //    voxelGrid.setInputCloud (cloud_msg_const_);
    //    voxelGrid.setLeafSize (0.01f, 0.01f, 0.01f);
    //    voxelGrid.filter (cloud_msg_);

    //Removing NANs from the pointcloud
    //    std::vector<int> mapp;
    //    pcl::removeNaNFromPointCloud(*cloud_msg_const_, cloud_msg_, mapp);

    //    cloud_n_->resize(0);
    //    for(size_t i =0; i < cloud_msg_const_->size(); ++i)
    //    {
    //        cloud_n_->points.push_back(cloud_msg_const_->points[i]);
    //    }cam0

    //    cloud_n_->height = msg->height;
    //    cloud_n_->width  = msg->width;
    //    cloud_n_->is_dense = false;

    //adding a pass through filter for removing noise from the pointcloud
    pcl::PassThrough<pcl::PointXYZ> pass;
    pass.setInputCloud(cloud_n_);
    pass.setFilterFieldName("z");
    pass.setFilterLimits(0.3, 3.0);
    pass.setKeepOrganized(true);
    pass.filter(*cloud_n_);

    //Downsample the point cloud
    cloud_n_ = organisedDownSampling(cloud_n_);

    normal_cloud_->clear();
    //compute the normals using integral normals method
    normal_cloud_ = normalSegmentation(cloud_n_);
    //regions_      = multiPlaneSegmentation(cloud_n_, normal_cloud_);

    //Method without kmeans
    //double altitude=computeGroundPlane(cloud_n_,normal_cloud_);

    //Method using kmeans
    computeKmeansGroundPlane(cloud_n_, normal_cloud_);


#ifdef debug_mode
    cout << "Time Delay in total " << (ros::Time::now() - msg->header.stamp).toSec() << endl;
#endif
}

void multiPlaneLocalization::rgbCamInfoCallback(const sensor_msgs::CameraInfo &msg)
{

    K_mat_.at<double>(0,0) = msg.K.at(0); K_mat_.at<double>(0,1) = msg.K.at(1); K_mat_.at<double>(0,2) = msg.K.at(2);
    K_mat_.at<double>(1,0) = msg.K.at(3); K_mat_.at<double>(1,1) = msg.K.at(4); K_mat_.at<double>(1,2) = msg.K.at(5);
    K_mat_.at<double>(2,0) = msg.K.at(6); K_mat_.at<double>(2,1) = msg.K.at(7); K_mat_.at<double>(2,2) = msg.K.at(8);

    //    dist_coeff_.at<double>(0,0) = msg.D.at(0);
    //    dist_coeff_.at<double>(1,0) = msg.D.at(1);
    //    dist_coeff_.at<double>(2,0) = msg.D.at(2);
    //    dist_coeff_.at<double>(3,0) = msg.D.at(3);
    //    dist_coeff_.at<double>(4,0) = msg.D.at(4);

    // camera matrix for robotics perception images
    K_mat_.at<double>(0,0) = 568.99; K_mat_.at<double>(0,1) = 0;      K_mat_.at<double>(0,2) = 640.0;
    K_mat_.at<double>(1,0) = 0;      K_mat_.at<double>(1,1) = 568.98; K_mat_.at<double>(1,2) = 480;
    K_mat_.at<double>(2,0) = 0;      K_mat_.at<double>(2,1) = 0;      K_mat_.at<double>(2,2) = 1;
    //    //std::cout << K_mat_ << std::endl;

    return;
}


void multiPlaneLocalization::rgbImageCallback(const sensor_msgs::Image &msg)
{

    //    try
    //    {
    //        cvFrontImage_ = cv_bridge::toCvCopy(msg,sensor_msgs::image_encodings::BGR8);

    //    }
    //    catch (cv_bridge::Exception& e)
    //    {

    //        ROS_ERROR("cv_bridge exception: %s", e.what());
    //        return;
    //    }

    //    if (image_counter_ ==0)
    //    {
    //        rgb_image_1_ = cvFrontImage_->image;
    //        image_counter_++;
    //        return;
    //    }

    //    rgb_image_2_ = cvFrontImage_->image;

    rgb_image_1_ = cv::imread("/home/hriday/Pictures/image1.jpg");
    rgb_image_2_ = cv::imread("/home/hriday/Pictures/image2.jpg");
    rgb_image_3_ = cv::imread("/home/hriday/Pictures/image3.jpg");

    std::vector<cv::KeyPoint> keypoints1, keypoints2, keypoints3;
    cv::Mat descriptors1, descriptors2, descriptors3;
    std::vector<cv::DMatch> good_matches12, good_matches21, symmetry_matches12;
    cv::Mat image_1_w_keypoints, image_2_w_keypoints, image_3_w_keypoints;

    camera_pose_extraction_obj_.computeImageFeatures(rgb_image_1_, keypoints1, descriptors1, image_1_w_keypoints);
    camera_pose_extraction_obj_.computeImageFeatures(rgb_image_2_, keypoints2, descriptors2, image_2_w_keypoints);

    //    imshow("ORB keypoints image 1", image_1_w_keypoints);
    //    imshow("ORB keypoints image 2", image_2_w_keypoints);
    //    cv::waitKey(1);

    good_matches12 = camera_pose_extraction_obj_.computeFeatureMatches(rgb_image_1_, descriptors1, descriptors2, keypoints1, keypoints2);
    good_matches21 = camera_pose_extraction_obj_.computeFeatureMatches(rgb_image_2_, descriptors2, descriptors1, keypoints2, keypoints1);

    //refine the matches
    camera_pose_extraction_obj_.symmetryTest(good_matches12, good_matches21, symmetry_matches12);

    //    for (int i = 0; i < good_matches.size(); i++)
    //        std::cout << "good matches " << good_matches[i].distance << std::endl;

    //-- Draw only "good" matches
    //        cv::Mat img_matches = cv::Mat::zeros(rgb_image_1_.rows, rgb_image_1_.cols, CV_64F);
    //        cv::drawMatches( rgb_image_1_, keypoints1, rgb_image_2_, keypoints2,
    //                         symmetry_matches12, img_matches, cv::Scalar::all(-1), cv::Scalar::all(-1),
    //                         vector<char>(), cv::DrawMatchesFlags::NOT_DRAW_SINGLE_POINTS );

    //        if(!img_matches.empty())
    //        {
    //            cv::imshow("Good Matches", img_matches);
    //            cv::waitKey(1);
    //        }
    //        else
    //            std::cout << "image matches empty " << std::endl;


    //calculate the matched image points
    std::vector<cv::Point2f> image_points_1, image_points_2;

    for (int i = 0; i < symmetry_matches12.size(); i++)
    {
        //query_idx is for left image and train_idx is for right image
        image_points_1.push_back(keypoints1[symmetry_matches12[i].queryIdx].pt);
        image_points_2.push_back(keypoints2[symmetry_matches12[i].trainIdx].pt);
    }

    std::cout << "image_points_1 size " << image_points_1.size() << std::endl;
    std::cout << "image_points_2 size " << image_points_2.size() << std::endl;

    //compute the fundamental matrix, the image matches are also refined based on the fundamental matrix
    cv:: Mat fundamental_mat;
    std::vector<cv::DMatch> refined_matches;
    fundamental_mat = camera_pose_extraction_obj_.computeFundamentalMatrix(image_points_1, image_points_2,
                                                                           symmetry_matches12, keypoints1, keypoints2, refined_matches);
    std::cout << "fundamental matrix refined " << fundamental_mat << std::endl;

    //assing class_id to all the matched keypoints
    for(int i = 0; i < refined_matches.size(); ++i)
    {
        //std::cout << "refined matches queryidx " << refined_matches[i].queryIdx << std::endl;
        //std::cout << "refined matches queryIdx pt " << keypoints1[refined_matches[i].queryIdx].pt << std::endl;ç
        keypoints1[refined_matches[i].queryIdx].class_id = i;
    }


    //refine the matched image points from the refined matches
    std::vector<cv::Point2f> ref_image_points_1, ref_image_points_2;

    for (int i = 0; i < refined_matches.size(); i++)
    {
        //query_idx is for left image and train_idx is for right image
        ref_image_points_1.push_back(keypoints1[refined_matches[i].queryIdx].pt);
        ref_image_points_2.push_back(keypoints2[refined_matches[i].trainIdx].pt);
    }

    //draw the epipolar lines
    //camera_pose_extraction_obj_.computeEpipolarlines(rgb_image_1_, rgb_image_2_, ref_image_points_1, ref_image_points_2, fundamental_mat);

    //compute the essential matrix
    cv::Mat essential_mat;
    essential_mat = camera_pose_extraction_obj_.computeEssentialMatrix(ref_image_points_1, ref_image_points_2, fundamental_mat, K_mat_);
    if(essential_mat.empty())
        return;
    std::cout << "essential matrix" << essential_mat << std::endl;

    //compute the relative rotation and translation matrices
    cv::Mat rotation_mat;
    cv::Mat translation_vec;
    camera_pose_extraction_obj_.computeRotationandTranslation(ref_image_points_1, ref_image_points_2, K_mat_, essential_mat, rotation_mat, translation_vec);
    std::cout << "rotation " << rotation_mat << std::endl;
    std::cout << "translation " << translation_vec << std::endl;

    //setting the rotation and translation of the first camera image to identity and zero resp.
    cv::Mat R1, T1;
    R1 = cv::Mat::eye(3,3, CV_64F);
    T1 = cv::Mat::zeros(3,1, CV_64F);

    //compute the 3D points using the 2 cam images
    cv::Mat points3D, re_points3D;
    camera_pose_extraction_obj_.computeLinearTriangulation(K_mat_, ref_image_points_1, ref_image_points_2, points3D, R1, T1, rotation_mat, translation_vec);
    //std::cout << "3D points " << points4D << std::endl;
    re_points3D = cv::Mat::zeros(points3D.cols, 3, CV_32F);

    //converting from homogenous coordinates to euclidean
    for(int i = 0; i < points3D.cols; ++i)
    {
        re_points3D.at<float>(i,0) = points3D.at<float>(0,i) / points3D.at<float>(3,i);
        re_points3D.at<float>(i,1) = points3D.at<float>(1,i) / points3D.at<float>(3,i);
        re_points3D.at<float>(i,2) = points3D.at<float>(2,i) / points3D.at<float>(3,i);
    }
    std::cout << "Point 3D " << re_points3D << std::endl;

    //--- Now starting the next loop of 3D-2D correspondence matching using the 3D points and 2D points of image3
    std::vector<cv::Point2f> image_points_3;
    std::vector<cv::KeyPoint> ref_keypoints1;
    cv::Mat ref_descriptors1;
    std::vector<cv::DMatch> good_matches13, good_matches31, symmetry_matches13;
    cv::Mat new_points3D = cv::Mat::zeros(0, 3, CV_32F);

    for (int i =0; i < refined_matches.size(); ++i)
        ref_keypoints1.push_back(keypoints1[refined_matches[i].queryIdx]);

    std::cout << "ref keypoints1 size " << ref_keypoints1.size() << std::endl;
    camera_pose_extraction_obj_.computeOnlyDescriptors(rgb_image_1_, ref_keypoints1, ref_descriptors1);
    std::cout << "ref descriptors size " << ref_descriptors1.size() << std::endl;

    camera_pose_extraction_obj_.computeImageFeatures(rgb_image_3_, keypoints3, descriptors3, image_3_w_keypoints);

    good_matches13 = camera_pose_extraction_obj_.computeFeatureMatches(rgb_image_1_, ref_descriptors1, descriptors3, ref_keypoints1, keypoints3);
    good_matches31 = camera_pose_extraction_obj_.computeFeatureMatches(rgb_image_3_, descriptors3, ref_descriptors1, keypoints3, ref_keypoints1);
    camera_pose_extraction_obj_.symmetryTest(good_matches13, good_matches31, symmetry_matches13);

    std::cout << "good_matches13 size " << good_matches13.size() << std::endl;
    std::cout << "symmetry_matches13 matches size " << symmetry_matches13.size() << std::endl;

    camera_pose_extraction_obj_.correspondences2D3D(re_points3D, symmetry_matches13, ref_keypoints1, keypoints3, image_points_1, image_points_3, new_points3D);

    cv::Mat R3, T3;
    camera_pose_extraction_obj_.sovlePnPRansac(image_points_3, new_points3D, K_mat_, dist_coeff_ , R3, T3);

    //    rgb_image_1_ = rgb_image_2_;

    return;

}

void multiPlaneLocalization::imuAnglesCallback(const sensor_msgs::ImuConstPtr &msg)
{

    tf::Quaternion tf_quaternion;
    tf_quaternion.setX(msg->orientation.x);
    tf_quaternion.setY(msg->orientation.y);
    tf_quaternion.setZ(msg->orientation.z);
    tf_quaternion.setW(msg->orientation.w);

    tf::Matrix3x3(tf_quaternion).getRPY(roll_, pitch_, yaw_);

    //    cout << "roll " << roll_ << endl
    //         << "pitch " << pitch_ << endlcam0
    //         << "yaw "   << yaw_   << endl;

    return;
}

void multiPlaneLocalization::viconAnglesCallback(const geometry_msgs::TransformStamped &msg)
{
    tf::Quaternion tf_quaternion;
    tf_quaternion.setX(msg.transform.rotation.x);
    tf_quaternion.setY(msg.transform.rotation.y);
    tf_quaternion.setZ(msg.transform.rotation.z);
    tf_quaternion.setW(msg.transform.rotation.w);

    tf::Matrix3x3(tf_quaternion).getRPY(roll_, pitch_, yaw_);

    roll_ = roll_;
    pitch_ = -pitch_;
    yaw_ = -yaw_;

    //    cout << "roll " << roll_ << endl
    //         << "pitch " << pitch_ << endl
    //         << "yaw "   << yaw_   << endl;

    return;


}

pcl::PointCloud<pcl::PointXYZ>::Ptr multiPlaneLocalization::organisedDownSampling(pcl::PointCloud<pcl::PointXYZ>::Ptr pointT)
{
    pcl::PointCloud<pcl::PointXYZ>::Ptr downsampled_cloud_ (new pcl::PointCloud<pcl::PointXYZ>);
    int num_row =1, new_width = pointT->width;

    downsampled_cloud_->resize(0);
    for(size_t i =0; i < pointT->size(); i+=2)
    {
        pcl::PointXYZ avg_points;


        if (i < new_width)
        {
            avg_points.x = (pointT->points[i].x + pointT->points[i+1].x +  pointT->points[i+pointT->width].x + pointT->points[i+(pointT->width+1)].x)/ 4;
            avg_points.y = (pointT->points[i].y + pointT->points[i+1].y +  pointT->points[i+pointT->width].y + pointT->points[i+(pointT->width+1)].y)/ 4;
            avg_points.z = (pointT->points[i].z + pointT->points[i+1].z +  pointT->points[i+pointT->width].z + pointT->points[i+(pointT->width+1)].z)/ 4;
            //            avg_points.r = (pointT->points[i].r + pointT->points[i+1].r +  pointT->points[i+pointT->width].r + pointT->points[i+(pointT->width+1)].r)/ 4;
            //            avg_points.g = 0.999089(pointT->points[i].g + pointT->points[i+1].g +  pointT->points[i+pointT->width].g + pointT->points[i+(pointT->width+1)].g)/ 4;
            //            avg_points.b = (pointT->points[i].b + pointT->points[i+1].b +  pointT->points[i+pointT->width].b + pointT->points[i+(pointT->width+1)].b)/ 4;

            downsampled_cloud_->push_back(avg_points);
        }
        else if (i == new_width)
        {

            if (i < pointT->size())
            {

                num_row = num_row+2;
                new_width = num_row*pointT->width;
                i = ((i+pointT->width) -2);
            }
            else
                break;

        }
        //cout << "i " << i << endl;

    }

    //    cout << "downsample cloud size " << downsampled_cloud_->size() << endl;
    //    cout << "cloud_n size " << pointT->size() << endl;

    downsampled_cloud_->width = pointT->width/2;
    downsampled_cloud_->height = pointT->height/2;

    return downsampled_cloud_;
}


pcl::PointCloud<pcl::Normal>::Ptr multiPlaneLocalization::normalSegmentation(pcl::PointCloud<pcl::PointXYZ>::Ptr pointT)
{
    pcl::IntegralImageNormalEstimation<pcl::PointXYZ, pcl::Normal> ne;
    ne.setNormalEstimationMethod (ne.SIMPLE_3D_GRADIENT);
    ne.setMaxDepthChangeFactor (0.03f);
    ne.setNormalSmoothingSize (20.0f);

    pcl::PointCloud<pcl::Normal>::Ptr normal_cloud (new pcl::PointCloud<pcl::Normal>);
    normal_cloud->clear();

    double normal_start = pcl::getTime ();
    ne.setInputCloud (pointT);
    ne.compute (*normal_cloud);
    double normal_end = pcl::getTime ();

#ifdef debug_mode
    //    std::cout << "\033[1;32mNormal Estimation took\033[0m\n " << double (normal_end - normal_start) << std::endl;
#endif
    //    kmeansSegViewer->removeAllPointClouds();
    //    //pcl::visualization::PointCloudColorHandlerRGBField<pcl::PointXYZ> rgb(kmeans_segmented_points);
    //    kmeansSegViewer->addPointCloud<pcl::PointXYZ>(pointT,"normal segmented cloud");
    //    kmeansSegViewer->setPointCloudRenderingProperties (pcl::visualization::PCL_VISUALIZER_POINT_SIZE, 1, "normal segmented cloud");
    //    kmeansSegViewer->addPointCloudNormals<pcl::PointXYZ , pcl::Normal >(pointT,normal_cloud, 10,0.05, "Normals" );

    return normal_cloud;


}

void multiPlaneLocalization::transformCameraToWorldFrame(Eigen::Matrix4f &transformation_matrix)
{
    Eigen::Matrix4f rot_x_cam, rot_x_robot, rot_z_robot, T_robot_world, translation_cam, T_test, T_test1;
    rot_x_cam.setZero(4,4), rot_x_robot.setZero(4,4), rot_z_robot.setZero(4,4), T_robot_world.setZero(4,4), translation_cam.setZero(4,4), T_test.setZero(4,4);

    rot_x_cam(0,0) = 1;
    rot_x_cam(1,1) =  cos(-camera_pitch_angle_);
    rot_x_cam(1,2) = -sin(-camera_pitch_angle_);
    rot_x_cam(2,1) =  sin(-camera_pitch_angle_);
    rot_x_cam(2,2) =  cos(-camera_pitch_angle_);
    rot_x_cam(3,3) = 1;

    //rotation of -90
    rot_x_robot(0,0) = 1;
    rot_x_robot(1,1) =  cos(-1.5708);
    rot_x_robot(1,2) = -sin(-1.5708);
    rot_x_robot(2,1) =  sin(-1.5708);
    rot_x_robot(2,2) =  cos(-1.5708);
    rot_x_robot(3,3) = 1;

    //rotation of -90
    rot_z_robot(0,0) = cos(-1.5708);
    rot_z_robot(0,1) = -sin(-1.5708);
    rot_z_robot(1,0) = sin(-1.5708);
    rot_z_robot(1,1) = cos(-1.5708);
    rot_z_robot(2,2) = 1;
    rot_z_robot(3,3) = 1;

    //tranlation of 15cm in
    translation_cam(0,0) = 1;
    translation_cam(0,3) = -0.15;

    translation_cam(1,1) = 1;
    translation_cam(2,2) = 1;
    translation_cam(3,3) = 1;


    //transformation from robot to world
    T_robot_world(0,0) = cos(yaw_)*cos(pitch_);
    T_robot_world(0,1) = cos(yaw_)*sin(pitch_)*sin(roll_) - sin(yaw_)*cos(roll_);
    T_robot_world(0,2) = cos(yaw_)*sin(pitch_)*cos(roll_) + sin(yaw_)*sin(pitch_);

    T_robot_world(1,0) = sin(yaw_)*cos(pitch_);
    T_robot_world(1,1) = sin(yaw_)*sin(pitch_)*sin(roll_) + cos(yaw_)*cos(roll_);
    T_robot_world(1,2) = sin(yaw_)*sin(pitch_)*cos(roll_) - cos(yaw_)*sin(roll_);

    T_robot_world(2,0) = -sin(pitch_);
    T_robot_world(2,1) = cos(pitch_)*sin(roll_);
    T_robot_world(2,2) = cos(pitch_)*cos(roll_);

    T_robot_world(0,3) = pose_x_;
    T_robot_world(1,3) = pose_y_;
    T_robot_world(2,3) = pose_z_;
    T_robot_world(3,3) = 1;


    transformation_matrix = T_robot_world * translation_cam * rot_z_robot * rot_x_robot * rot_x_cam;

    //Will only be executed when testing with euroc dataset
    if(use_dataset_euroc)
    {
        //for testing the euroc data set vicon transform
        T_test(0,0) = 0.33638;
        T_test(0,1) = -0.01749;
        T_test(0,2) = 0.94156;

        T_test(1,0) = -0.02078;
        T_test(1,1) = -0.99972;
        T_test(1,2) = -0.01114;

        T_test(2,0) = 0.94150;
        T_test(2,1) = -0.01582;
        T_test(2,2) = -0.33665;

        T_test(0,3) = 0.06901;
        T_test(1,3) = -0.02781;
        T_test(2,3) = -0.12395;
        T_test(3,3) = 1;


        //for testing the euroc dataset camera to imu transform
        T_test1(0,0) = 0.0148655429818;
        T_test1(0,1) = -0.999880929698;
        T_test1(0,2) = 0.00414029679422;

        T_test1(1,0) = 0.999557249008;
        T_test1(1,1) = 0.0149672133247;
        T_test1(1,2) = 0.025715529948;

        T_test1(2,0) = -0.0257744366974;
        T_test1(2,1) = 0.00375618835797;
        T_test1(2,2) = 0.999660727178;

        T_test1(0,3) = -0.0216401454975;
        T_test1(1,3) = -0.064676986768;
        T_test1(2,3) = 0.00981073058949;
        T_test1(3,3) = 1;

        transformation_matrix = T_robot_world * T_test.transpose().eval() * T_test1;
    }


    //cout << "rot_x_cam " << rot_x_cam << endl;
    //cout << "rot_x_robot" << rot_x_robot << endl;
    //cout << "rot_z_robot" << rot_z_robot << endl;
    //cout << "T_robot_world" << T_robot_world << endl;
    //cout << "T_test " << T_test << endl;
    //cout << "transformation matrix " << transformation_matrix << endl;
}


inline void multiPlaneLocalization::publishRefinedAltitude(float refined_altitude, ros::Time time_stamp)
{
    geometry_msgs::PoseStamped altiutude_cam;

    altiutude_cam.header.stamp = time_stamp;
    altiutude_cam.pose.position.z = (refined_altitude);
    altitude_in_camera_pub_.publish(altiutude_cam);
    publish_altitude_ = false;

    return;
}

