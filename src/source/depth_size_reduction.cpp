#include "depth_size_reduction.h"


depthSizeReduction::depthSizeReduction(int argc,char **argv)
{
    std::cout << "opening depth size reduction node " << std::endl;
}

depthSizeReduction::~depthSizeReduction()
{
    std::cout << "destroying depth size reduction node " << std::endl;
}

bool depthSizeReduction::run()
{
    while(ros::ok())
    {
        ros::spinOnce();
    }

}

void depthSizeReduction::open(ros::NodeHandle n)
{
    image_transport::ImageTransport it(n);

    depth_image_subscriber_       = n.subscribe("/camera/depth/image_raw",1, &depthSizeReduction::depthImagecallback, this);
    depth_cam_info_subscriber_    = n.subscribe("/camera/depth/camera_info", 1, &depthSizeReduction::depthCamInfoCallback, this);
    ir_cam_right_image_subsriber_ = n.subscribe("/camera/infrared2/image_raw",1, &depthSizeReduction::irRightCamImageCallback, this);
    ir_cam_left_image_subsriber_  = n.subscribe("/camera/infrared1/image_raw",1, &depthSizeReduction::irLeftCamImageCallback, this);

    cam_left_image_subsriber_     = n.subscribe("/cam0/image_raw",1, &depthSizeReduction::LeftCamImageCallback, this);
    cam_right_image_subsriber_     = n.subscribe("/cam1/image_raw",1, &depthSizeReduction::RightCamImageCallback, this);


    downsampled_depth_image_pub_    = it.advertise("downsampled_depth/image_raw", 1);
    downsampled_ir_right_image_pub_ = it.advertise("downsampled_ir_right/image_raw",1);
    downsampled_ir_left_image_pub_  = it.advertise("downsampled_ir_left/image_raw",1);
    depth_cam_info_pub_             = n.advertise<sensor_msgs::CameraInfo>("downsampled_depth/camera_info",1);
    downsampled_point_cloud_        = n.advertise< pcl::PointCloud <pcl::PointXYZ> >("downsampled_point_cloud", 1);

    left_cam_image_pub_             = it.advertise("/stereo/left/image_raw", 1);
    right_cam_image_pub_            = it.advertise("/stereo/right/image_raw",1);

    left_cam_info_pub_              = n.advertise<sensor_msgs::CameraInfo>("/stereo/left/camera_info",1);
    right_cam_info_pub_             = n.advertise<sensor_msgs::CameraInfo>("/stereo/right/camera_info",1);

    return;
}

void depthSizeReduction::depthImagecallback(const sensor_msgs::Image &msg)
{

    cv_bridge::CvImagePtr cv_ptr;
    cv::Mat depth_image;

    try
    {
        cv_ptr = cv_bridge::toCvCopy(msg,sensor_msgs::image_encodings::TYPE_16UC1);

    }
    catch (cv_bridge::Exception& e)
    {

        ROS_ERROR("cv_bridge exception: %s", e.what());
        return;
    }

    depth_image = cv_ptr->image;

    //    if(!depth_image.empty())
    //    {
    //        cv::imshow("depth image opencv", depth_image);
    //        cv::waitKey(1);
    //    }

    //cv::pyrDown(depth_image, depth_image, cv::Size(200, 150));
    cv::resize(depth_image, depth_image, cv::Size(200, 150));

    //    if(!depth_image.empty())
    //    {
    //        cv::imshow("depth image opencv after pyrdown", depth_image);
    //        cv::waitKey(1);
    //    }

    sensor_msgs::ImagePtr depth_image_ptr = cv_bridge::CvImage(std_msgs::Header(), "16UC1", depth_image).toImageMsg();
    depth_image_ptr->header.frame_id = msg.header.frame_id;
    depth_image_ptr->header.stamp = msg.header.stamp;
    depth_image_ptr->header.seq = msg.header.seq;

    downsampled_depth_image_pub_.publish(depth_image_ptr);

    return;

}

void depthSizeReduction::irRightCamImageCallback(const sensor_msgs::Image &msg)
{
    cv_bridge::CvImagePtr cv_ptr;
    cv::Mat ir_image;

    try
    {
        cv_ptr = cv_bridge::toCvCopy(msg,sensor_msgs::image_encodings::TYPE_8UC1);

    }
    catch (cv_bridge::Exception& e)
    {

        ROS_ERROR("cv_bridge exception: %s", e.what());
        return;
    }

    ir_image = cv_ptr->image;

    //    if(!depth_image.empty())
    //    {
    //        cv::imshow("depth image opencv", depth_image);
    //        cv::waitKey(1);
    //    }

    //cv::pyrDown(depth_image, depth_image, cv::Size(depth_image.cols/2, depth_image.rows/2));
    //cv::resize(ir_image, ir_image, cv::Size(ir_image.cols, ir_image.rows));

    //    if(!depth_image.empty())
    //    {
    //        cv::imshow("depth image opencv after pyrdown", depth_image);
    //        cv::waitKey(1);
    //    }


    sensor_msgs::ImagePtr ir_image_ptr = cv_bridge::CvImage(std_msgs::Header(), "mono8", ir_image).toImageMsg();
    ir_image_ptr->header.frame_id = msg.header.frame_id;
    ir_image_ptr->header.stamp = msg.header.stamp;
    ir_image_ptr->header.seq = msg.header.seq;


    downsampled_ir_right_image_pub_.publish(ir_image_ptr);

}

void depthSizeReduction::irLeftCamImageCallback(const sensor_msgs::Image &msg)
{
    cv_bridge::CvImagePtr cv_ptr;
    cv::Mat ir_image;

    try
    {
        cv_ptr = cv_bridge::toCvCopy(msg,sensor_msgs::image_encodings::TYPE_8UC1);

    }
    catch (cv_bridge::Exception& e)
    {

        ROS_ERROR("cv_bridge exception: %s", e.what());
        return;
    }

    ir_image = cv_ptr->image;

    //    if(!depth_image.empty())
    //    {
    //        cv::imshow("depth image opencv", depth_image);
    //        cv::waitKey(1);
    //    }

    //cv::pyrDown(depth_image, depth_image, cv::Size(depth_image.cols/2, depth_image.rows/2));
    cv::resize(ir_image, ir_image, cv::Size(ir_image.cols/2, ir_image.rows/2));

    //    if(!depth_image.empty())
    //    {
    //        cv::imshow("depth image opencv after pyrdown", depth_image);
    //        cv::waitKey(1);
    //    }


    sensor_msgs::ImagePtr ir_image_ptr = cv_bridge::CvImage(std_msgs::Header(), "mono8", ir_image).toImageMsg();
    ir_image_ptr->header.frame_id = msg.header.frame_id;
    ir_image_ptr->header.stamp = msg.header.stamp;
    ir_image_ptr->header.seq = msg.header.seq;


    downsampled_ir_left_image_pub_.publish(ir_image_ptr);

}

void depthSizeReduction::depthCamInfoCallback(const sensor_msgs::CameraInfo &msg)
{

    sensor_msgs::CameraInfo depth_cam_info;

    depth_cam_info.header.stamp = msg.header.stamp;
    depth_cam_info.header.frame_id = msg.header.frame_id;

    depth_cam_info.height = msg.height/2;
    depth_cam_info.width  = msg.width/2;

    depth_cam_info.distortion_model = msg.distortion_model;

    depth_cam_info.D = msg.D;

    depth_cam_info.K.at(0) = 156.7376;
    depth_cam_info.K.at(2) = 80.58;
    depth_cam_info.K.at(4) = 156.3636;
    depth_cam_info.K.at(5) = 59.10;
    depth_cam_info.K.at(8) = 1.0;

    depth_cam_info.R.at(0) = 1.0;
    depth_cam_info.R.at(4) = 1.0;
    depth_cam_info.R.at(8) = 1.0;

    depth_cam_info.P.at(0) = 163.81;
    depth_cam_info.P.at(2) = 89.97;
    depth_cam_info.P.at(5) = 163.812;
    depth_cam_info.P.at(6) = 58.18;
    depth_cam_info.P.at(10) = 1.0;

    //    //REMOVE THIS LATER
    //    depth_cam_info.K = msg.K;
    //    depth_cam_info.R = msg.R;
    //    depth_cam_info.P = msg.P;

    //    depth_cam_info.roi.do_rectify = msg.roi.do_rectify;

    depth_cam_info_pub_.publish(depth_cam_info);

}

void depthSizeReduction::LeftCamImageCallback(const sensor_msgs::Image &msg)
{
    sensor_msgs::CameraInfo left_cam_info;

    left_cam_info.header.stamp = msg.header.stamp;
    left_cam_info.header.frame_id = "left_cam";

    left_cam_info.height = msg.height;
    left_cam_info.width  = msg.width;

    left_cam_info.distortion_model = "plumb_bob";

    left_cam_info.D.resize(5);
    left_cam_info.D.at(0) = -0.283587;
    left_cam_info.D.at(1) = 0.073431;
    left_cam_info.D.at(2) = 0.000256;
    left_cam_info.D.at(3) = -0.000118;
    left_cam_info.D.at(4) = 0;

    left_cam_info.K.at(0) = 462.500314;
    left_cam_info.K.at(2) = 365.884421;
    left_cam_info.K.at(4) = 461.421843;
    left_cam_info.K.at(5) = 246.908857;
    left_cam_info.K.at(8) = 1.0;

    left_cam_info.R.at(0) = 0.999996;
    left_cam_info.R.at(1) = -0.001640;
    left_cam_info.R.at(2) = 0.002339;
    left_cam_info.R.at(3) = 0.001622;
    left_cam_info.R.at(4) = 0.999968;
    left_cam_info.R.at(5) = 0.007816;
    left_cam_info.R.at(6) = -0.002352;
    left_cam_info.R.at(7) = -0.007812;
    left_cam_info.R.at(8) = 0.999967;

    left_cam_info.P.at(0) = 439.229301;
    left_cam_info.P.at(2) = 371.292057;
    left_cam_info.P.at(5) = 439.229301;
    left_cam_info.P.at(6) = 250.439064;
    left_cam_info.P.at(10) = 1.0;

    left_cam_image_pub_.publish(msg);
    left_cam_info_pub_.publish(left_cam_info);

}


void depthSizeReduction::RightCamImageCallback(const sensor_msgs::Image &msg)
{
    sensor_msgs::CameraInfo right_cam_info;

    right_cam_info.header.stamp = msg.header.stamp;
    right_cam_info.header.frame_id = "right_cam";

    right_cam_info.height = msg.height;
    right_cam_info.width  = msg.width;

    right_cam_info.distortion_model = "plumb_bob";

    right_cam_info.D.resize(5);
    right_cam_info.D.at(0) = -0.28368365;
    right_cam_info.D.at(1) = 0.07451284;
    right_cam_info.D.at(2) = -0.00010473;
    right_cam_info.D.at(3) = -3.55590700e-05;
    right_cam_info.D.at(4) = 0;

    right_cam_info.K.at(0) = 461.539699;
    right_cam_info.K.at(2) = 374.123628;
    right_cam_info.K.at(4) = 460.696401;
    right_cam_info.K.at(5) = 253.036470;
    right_cam_info.K.at(8) = 1.0;

    right_cam_info.R.at(0) = 0.999960;
    right_cam_info.R.at(1) = -0.004132;
    right_cam_info.R.at(2) = -0.007989;
    right_cam_info.R.at(3) = 0.004069;
    right_cam_info.R.at(4) = 0.999961;
    right_cam_info.R.at(5) = -0.007830;
    right_cam_info.R.at(6) = 0.008021;
    right_cam_info.R.at(7) = 0.007798;
    right_cam_info.R.at(8) = 0.999937;

    right_cam_info.P.at(0) = 439.229301;
    right_cam_info.P.at(2) = 371.292057;
    right_cam_info.P.at(3) = -48.149219;
    right_cam_info.P.at(5) = 439.229301;
    right_cam_info.P.at(6) = 250.439064;
    right_cam_info.P.at(10) = 1.0;


    right_cam_image_pub_.publish(msg);
    right_cam_info_pub_.publish(right_cam_info);

}











