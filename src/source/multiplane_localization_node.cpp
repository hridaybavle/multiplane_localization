
//I/O stream
//std::cout
#include <iostream>

//ROS
#include "ros/ros.h"



#include "multiplane_localization.h"



using namespace std;


int main(int argc,char **argv)
{

    ros::init(argc, argv, "altitude_from_rgbd");
    ros::NodeHandle nh;

    multiPlaneLocalization myMultiPlaneLocalization(argc,argv);
    myMultiPlaneLocalization.open(nh);

    myMultiPlaneLocalization.run();

    return 0;
}
