
//I/O stream
//std::cout
#include <iostream>

//ROS
#include "ros/ros.h"



#include "depth_size_reduction.h"



using namespace std;


int main(int argc,char **argv)
{

    ros::init(argc, argv, "depth_size_reduction");
    ros::NodeHandle nh;

    depthSizeReduction myDepthSizeReduction(argc,argv);
    myDepthSizeReduction.open(nh);

    myDepthSizeReduction.run();

    return 0;
}
