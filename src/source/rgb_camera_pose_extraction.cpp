#include "rgb_camera_pose_extraction.h"

rgb_camera_pose_extraction::rgb_camera_pose_extraction()
{
    std::cout << "rgb_camera_pose_extraction " << std::endl;
}


rgb_camera_pose_extraction::~rgb_camera_pose_extraction()
{
    std::cout << "closing rgb_camera_pose_extraction" << std::endl;
}

void rgb_camera_pose_extraction::computeImageFeatures(cv::Mat image, std::vector<cv::KeyPoint>& keypoints, cv::Mat& descriptors, cv::Mat& output_image)
{

    //std::cout << "entered in the feature extraction func" << std::endl;
    cv::Mat grey_image;
    //out_image.zeros(image.cols, image.rows, CV_16SC3);

    //conver the color image to greyscale
    cv::cvtColor(image, grey_image, cv::COLOR_RGB2GRAY);

    //creating an orb detector
    //cv::Ptr<cv::FeatureDetector> detector = cv::ORB::create();

    //SIFT feature detector
    cv::Ptr<cv::FeatureDetector> detector = cv::xfeatures2d::SIFT::create();
    int flags;
    const cv::Scalar color=(0,255,0);

    //Keypoint detector using ORB
    detector->detect(grey_image,keypoints);

    //drawing the keypoints in out_image
    cv::drawKeypoints(grey_image, keypoints, output_image, color, flags | cv::DrawMatchesFlags::DEFAULT );

    //orb descriptor extractor
    //cv::Ptr<cv::DescriptorExtractor> extractor = cv::ORB::create();

    //SIFT descriptor extractor
    cv::Ptr<cv::DescriptorExtractor> extractor = cv::xfeatures2d::SIFT::create();

    extractor->compute(grey_image, keypoints, descriptors);

    //std::cout << "Descriptor " << descriptors << std::endl;
}

void rgb_camera_pose_extraction::computeOnlyDescriptors(cv::Mat image, std::vector<cv::KeyPoint> keypoints, cv::Mat &descriptors)
{

    //std::cout << "entered in the feature extraction func" << std::endl;
    cv::Mat grey_image;
    //out_image.zeros(image.cols, image.rows, CV_16SC3);

    //conver the color image to greyscale
    cv::cvtColor(image, grey_image, cv::COLOR_RGB2GRAY);

    //orb descriptor extractor
    //cv::Ptr<cv::DescriptorExtractor> extractor = cv::ORB::create();

    //SIFT descriptor
    cv::Ptr<cv::DescriptorExtractor> extractor = cv::xfeatures2d::SiftDescriptorExtractor::create();

    extractor->compute(grey_image, keypoints, descriptors);


}

std::vector<cv::DMatch> rgb_camera_pose_extraction::computeFeatureMatches(cv::Mat left_image, cv::Mat descriptor_1, cv::Mat descriptor_2,
                                                                          std::vector<cv::KeyPoint> keypoints_1, std::vector<cv::KeyPoint> keypoints_2)
{
    //see https://docs.opencv.org/3.2.0/d5/d6f/tutorial_feature_flann_matcher.html for example of flan based matcher
    std::vector<cv::DMatch> good_matches;

    cv::Ptr<cv::BFMatcher> matcher = cv::BFMatcher::create();

    //FLANN_INDEX_LSH = 6
    //cv::FlannBasedMatcher matcherFlann(new cv::flann::LshIndexParams(20,10,2));
    //matching for SIFT descriptor
    cv::FlannBasedMatcher matcherFlann(new cv::flann::KDTreeIndexParams(5));

    std::vector<std::vector<cv::DMatch> > matches;
    //matcher->knnmatch(descriptor_1, descriptor_2, matches, 2);

    //flan based matcher
    matcherFlann.knnMatch(descriptor_1, descriptor_2, matches, 2);


    //Extracting good matches super simple method and not working properly
    //    double max_dist = 0; double min_dist = 100;
    //-- Quick calculation of max and min distances between keypoints
    //    for( int i = 0; i < descriptor_1.rows; i++ )
    //    {
    //        double dist = matches[i].distance;
    //        if( dist < min_dist ) min_dist = dist;
    //        if( dist > max_dist ) max_dist = dist;
    //    }
    //    std::cout << "min dist " << min_dist << std::endl;
    //    std::cout << "max dist " << max_dist << std::endl;

    //    int thres_dist;
    //    float sum = 0;
    //    for (int i =0; i < matches.size(); ++i)
    //    {
    //        sum += matches[i].distance;
    //    }

    //    std::cout << "size matches " << descriptor_1.size() << std::endl;
    //    std::cout << "sum " << sum << std::endl;
    //    thres_dist = ((sum) / matches.size()) * 0.65;
    //    std::cout << "thres_dist " << thres_dist << std::endl;

    //    for( int i = 0; i < descriptor_1.rows; i++ )
    //    {
    //        if( matches[i].distance <= thres_dist)
    //        {
    //            good_matches.push_back( matches[i]);
    //        }

    //    }

    //Method of David Bowe checking if the ratio of matches is less than 0.8
    //This method seems to work the best
    good_matches.clear();
    const float ratio = 0.80;
    for (int i = 0; i < matches.size(); ++i)
    {
        if(matches[i][0].distance < ratio * matches[i][1].distance)
            good_matches.push_back(matches[i][0]);
    }


    //Method found on stackoverflow
    //    double thres_dist = 0.25 * sqrt(double(left_image.size().height * left_image.size().height + left_image.size().width * left_image.size().width));
    //    std::cout << "thres dist " << thres_dist << std::endl;

    //    for (int i = 0; i < matches.size(); ++i)
    //    {
    //        for (int j = 0; j < matches[i].size(); ++j)
    //        {
    //            cv::Point2f from = keypoints_1[matches[i][j].queryIdx].pt;
    //            cv::Point2f to   = keypoints_2[matches[i][j].trainIdx].pt;

    //            //calculate local distance for each possible match
    //            double dist = sqrt((from.x - to.x) * (from.x - to.x) + (from.y - to.y) * (from.y - to.y));

    //            //std::cout << "distance " << dist << std::endl;
    //            //save as best match if local distance is in specified area and on same height
    //            if (dist < thres_dist && abs(from.y-to.y)<5)
    //            {
    //                good_matches.push_back(matches[i][j]);
    //                j = matches[i].size();
    //            }

    //        }
    //    }


    return good_matches;
}

void rgb_camera_pose_extraction::symmetryTest(const std::vector<cv::DMatch> &matches1, const std::vector<cv::DMatch> &matches2, std::vector<cv::DMatch>& sym_matches)
{

    // for all matches image 1 -> image 2
    for (int i = 0; i < matches1.size() ; ++i)
    {
        // ignore deleted matches
        //if (i < 2)
        //    continue;

        // for all matches image 2 -> image 1
        for (int j = 0; j < matches2.size(); ++j)
        {
            // ignore deleted matches
            //if (j < 2)
            //    continue;
            // Match symmetry test
            if ( matches1[i].queryIdx == matches2[j].trainIdx  &&
                 matches2[j].queryIdx == matches1[i].trainIdx)
            {
                // add symmetrical match
                sym_matches.push_back(cv::DMatch(matches1[i].queryIdx,
                                                 matches1[i].trainIdx,
                                                 matches1[i].distance));
                break; // next match in image 1 -> image 2
            }
        }
    }


}

cv::Mat rgb_camera_pose_extraction::computeFundamentalMatrix(std::vector<cv::Point2f>& points_1, std::vector<cv::Point2f>& points_2,
                                                             std::vector<cv::DMatch> matches, std::vector<cv::KeyPoint> keypoints_1,
                                                             std::vector<cv::KeyPoint> keypoints_2, std::vector<cv::DMatch>& out_matches)
{
    cv::Mat fundamental_matrix, fundamental_matrix_refined;
    float distance = 0.5, confidence =0.99;
    std::vector<uchar> inliers(points_1.size(),1);
    //std::vector<cv::DMatch> out_matches;

    fundamental_matrix= cv::findFundamentalMat(points_1,points_2, inliers, CV_FM_RANSAC, distance, confidence);
    std::cout << "fundamental matrix " << fundamental_matrix << std::endl;

    // for all matches
    for (int i =0; i < inliers.size(); ++i)
    {
        if(inliers[i])
            out_matches.push_back(matches[i]);
        //        if (*itIn)
        //            out_matches.push_back(*itM);
    }

    //--Refine the fundamental matrix --//
    points_1.clear(), points_2.clear();

    for (int i = 0; i < out_matches.size(); i++)
    {
        //query_idx is for left image and train_idx is for right image
        points_1.push_back(keypoints_1[out_matches[i].queryIdx].pt);
        points_2.push_back(keypoints_2[out_matches[i].trainIdx].pt);
    }

    std::cout << "new points_1 size " << points_1.size() << std::endl;
    std::cout << "new points_2 size " << points_2.size() << std::endl;


    if(points_1.size() < 8 || points_2.size() < 8)
        return fundamental_matrix;

    fundamental_matrix_refined = cv::findFundamentalMat(points_1, points_2, cv::FM_8POINT, 0.1, 0.99);

    return fundamental_matrix_refined;


    //    cv::Mat fundamental_mat, refined_fundamental_mat, F;
    //    fundamental_mat = cv::Mat::zeros(3,3, CV_64F);
    //    F = cv::Mat::zeros(points_1.size(),9,CV_64F);

    //    //F is Nx9 matrix, N is the length of the points
    //    for (int i =0; i < points_1.size(); ++i)
    //    {
    //        F.at<double>(i,0) = points_1.at(i).x*points_2.at(i).x;
    //        F.at<double>(i,1) = points_1.at(i).x*points_2.at(i).y;
    //        F.at<double>(i,2) = points_1.at(i).x;
    //        F.at<double>(i,3) = points_1.at(i).y*points_2.at(i).x;
    //        F.at<double>(i,4) = points_1.at(i).y*points_2.at(i).y;
    //        F.at<double>(i,5) = points_1.at(i).y;
    //        F.at<double>(i,6) = points_2.at(i).x;
    //        F.at<double>(i,7) = points_2.at(i).y;
    //        F.at<double>(i,8) = 1;

    //    }

    //    std::cout << "F " << F << std::endl;
    //    cv::SVD fundamental_svd(F, cv::SVD::MODIFY_A);

    //    //std::cout << "svd V " << fundamental_svd.vt << std::endl;
    //    //std::cout << "svd V size " << fundamental_svd.vt.size() << std::endl;

    //    fundamental_mat.at<double>(0,0) = fundamental_svd.vt.at<double>(0,8); fundamental_mat.at<double>(0,1) = fundamental_svd.vt.at<double>(1,8); fundamental_mat.at<double>(0,2) = fundamental_svd.vt.at<double>(2,8);
    //    fundamental_mat.at<double>(1,0) = fundamental_svd.vt.at<double>(3,8); fundamental_mat.at<double>(1,1) = fundamental_svd.vt.at<double>(4,8); fundamental_mat.at<double>(1,2) = fundamental_svd.vt.at<double>(5,8);
    //    fundamental_mat.at<double>(2,0) = fundamental_svd.vt.at<double>(6,8); fundamental_mat.at<double>(2,1) = fundamental_svd.vt.at<double>(7,8); fundamental_mat.at<double>(2,2) = fundamental_svd.vt.at<double>(8,8);

    //    std::cout << "fundamental matrix " << fundamental_mat << std::endl;

    //    //refining the fundamental matrix
    //    cv::SVD refined_fundamental_svd(fundamental_mat, cv::SVD::MODIFY_A);

    //    double d1, d2, d3;
    //    d1 = refined_fundamental_svd.w.at<double>(0);
    //    d2 = refined_fundamental_svd.w.at<double>(1);
    //    d3 = 0;

    //    cv::Matx33d W(d1,0,0,0,d2,0,0,0,d3);

    //    std::cout << "W " << W << std:: endl;

    //    refined_fundamental_mat = refined_fundamental_svd.u * cv::Mat(W) * refined_fundamental_svd.vt.t();
    //    refined_fundamental_mat = refined_fundamental_mat / cv::norm(refined_fundamental_mat);

    //    return refined_fundamental_mat;


}

void rgb_camera_pose_extraction::computeEpipolarlines(cv::Mat image_1, cv::Mat image_2, std::vector<cv::Point2f> image_points_1, std::vector<cv::Point2f> image_points_2,
                                                      cv::Mat fundamental_mat)
{

    std::vector<cv::Vec3f> epilines_1;
    cv::computeCorrespondEpilines(cv::Mat(image_points_1), 1, fundamental_mat, epilines_1);

    for(size_t i =0; i < epilines_1.size(); ++i)
    {
        cv::Scalar color(i*100, i*200, i*15);
        cv::line(image_2,
                 cv::Point(0, -epilines_1[i][2]/epilines_1[i][1]),
                cv::Point(image_1.cols,-(epilines_1[i][2]+epilines_1[i][0]*image_1.cols)/epilines_1[i][1]),
                color);
        cv::circle(image_2, image_points_2[i], 3, color, -1, CV_AA);
    }

    cv::imshow("epipolar_lines", image_2);
    cv::waitKey(1);
}

cv::Mat rgb_camera_pose_extraction::computeEssentialMatrix(std::vector<cv::Point2f> points_1, std::vector<cv::Point2f> points_2,
                                                           cv::Mat fundamental_matrix, cv::Mat K)
{
    cv::Mat essential_mat;

    essential_mat = K.t() * fundamental_matrix * K;

    cv::SVD svd(essential_mat, cv::SVD::MODIFY_A);

    cv::Matx33d W(1,0,0,0,1,0,0,0,0);

    //    std::cout << "svd d refined " << svd_d << std::endl;
    //    std::cout << "svd u " << svd.u  << std::endl;
    //    std::cout << "svd vt" << svd.vt << std::endl;

    essential_mat = svd.u * cv::Mat(W) * svd.vt.t();
    essential_mat = essential_mat / cv::norm(essential_mat);

    //    return essential_mat;

    //    cv::Mat essential_mat, mask;
    //    essential_mat = cv::findEssentialMat(points_1, points_2, K,cv::RANSAC, 0.99, 2, mask);

    return essential_mat;

}


void rgb_camera_pose_extraction::computeRotationandTranslation(std::vector<cv::Point2f>& points_1, std::vector<cv::Point2f>& points_2,
                                                               cv::Mat K, cv::Mat essential_mat, cv::Mat& rotation_mat, cv::Mat& translation_vec)
{
    //decompose the essential matrix
    cv::SVD svd(essential_mat, cv::SVD::MODIFY_A);

    cv::Mat svd_u  = svd.u;
    cv::Mat svd_vt = svd.vt;
    cv::Mat svd_w  = svd.w;

    cv::Matx33d W(0,-1,0,1,0,0,0,0,1);

    //std::cout << "W " << W << std::endl;

    rotation_mat    = svd_u * cv::Mat(W) * svd_vt.t();
    translation_vec = svd_u.col(2);

    std::cout << "rotation from matlab method " << rotation_mat << std::endl;
    std::cout << "translation from matlab method " << translation_vec << std::endl;

    cv::Mat mask, R1;
    cv::correctMatches(essential_mat, points_1, points_2, points_1, points_2);

    cv::decomposeEssentialMat(essential_mat,R1, rotation_mat, translation_vec);
    cv::recoverPose(essential_mat, points_1, points_2, K, rotation_mat, translation_vec, mask);

    //    std::cout << "R1 " << R1 << std::endl;
    //    std::cout << "R2 " << rotation_mat << std::endl;
}


void rgb_camera_pose_extraction::computeLinearTriangulation(cv::Mat K, std::vector<cv::Point2f> points_1, std::vector<cv::Point2f> points_2, cv::Mat& points3D,
                                                            cv::Mat R1, cv::Mat T1, cv::Mat R2, cv::Mat T2)
{

    cv::Mat P1 = cv::Mat::zeros(3,4, CV_64F);
    cv::Mat P2 = cv::Mat::zeros(3,4, CV_64F);
    cv::Mat dist_coeff = cv::Mat::zeros(4, 1, CV_64F);
    cv::Mat rect_mat;

    P1.at<double>(0,0) = R1.at<double>(0,0); P1.at<double>(0,1) = R1.at<double>(0,1); P1.at<double>(0,2) = R1.at<double>(0,2);P1.at<double>(0,3) = T1.at<double>(0);
    P1.at<double>(1,0) = R1.at<double>(1,0); P1.at<double>(1,1) = R1.at<double>(1,1); P1.at<double>(1,2) = R1.at<double>(1,2);P1.at<double>(1,3) = T1.at<double>(1);
    P1.at<double>(2,0) = R1.at<double>(2,0); P1.at<double>(2,1) = R1.at<double>(2,1); P1.at<double>(2,2) = R1.at<double>(2,2);P1.at<double>(2,3) = T1.at<double>(2);

    P2.at<double>(0,0) = R2.at<double>(0,0); P2.at<double>(0,1) = R2.at<double>(0,1); P2.at<double>(0,2) = R2.at<double>(0,2);P2.at<double>(0,3) = T2.at<double>(0);
    P2.at<double>(1,0) = R1.at<double>(1,0); P2.at<double>(1,1) = R2.at<double>(1,1); P2.at<double>(1,2) = R2.at<double>(1,2);P2.at<double>(1,3) = T2.at<double>(1);
    P2.at<double>(2,0) = R1.at<double>(2,0); P2.at<double>(2,1) = R2.at<double>(2,1); P2.at<double>(2,2) = R2.at<double>(2,2);P2.at<double>(2,3) = T2.at<double>(2);

    P1 = K*P1;
    P2 = K*P2;

    std::cout << "P1 " << P1 << std::endl;
    std::cout << "P2 " << P2 << std::endl;

    //points3D = cv::Mat::zeros(4, points_1.size(), CV_32F);
    //points3D         = cv::Mat::zeros(3, points_1.size(), CV_32F);

    //undistorting the points

    //cv::Mat points_1_new = cv::Mat(points_1.size(), 1, CV_64FC2);
    //cv::Mat points_2_new = cv::Mat(points_2.size(), 1, CV_64FC2);

    cv::undistortPoints(points_1, points_1, K, dist_coeff,rect_mat, P1);
    cv::undistortPoints(points_2, points_2, K, dist_coeff,rect_mat, P2);

    cv::triangulatePoints(P1, P2, points_1, points_2, points3D);

    //std::cout << "points4D " << points3D << std::endl;

    //    //method explained in the robotics perception course
    //    cv::Mat points_1_mat = cv::Mat::zeros(3,3, CV_64F);
    //    cv::Mat points_2_mat = cv::Mat::zeros(3,3, CV_64F);
    //    cv::Mat points3d_new_method(points_1.size(), 3, CV_64F);

    //    for (size_t i =0; i < points_1.size(); ++i)
    //    {
    //        points_1_mat.at<double>(0,0) = 0;              points_1_mat.at<double>(0,1) = -1;            points_1_mat.at<double>(0,2) = points_1[i].y;
    //        points_1_mat.at<double>(1,0) = 1;              points_1_mat.at<double>(1,1) = 0;             points_1_mat.at<double>(1,2) = -points_1[i].x;
    //        points_1_mat.at<double>(2,0) = -points_1[i].y; points_1_mat.at<double>(2,1) = points_1[i].x; points_1_mat.at<double>(2,2) = 0;

    //        points_2_mat.at<double>(0,0) = 0;              points_2_mat.at<double>(0,1) = -1;            points_2_mat.at<double>(0,2) = points_2[i].y;
    //        points_2_mat.at<double>(1,0) = 1;              points_2_mat.at<double>(1,1) = 0;             points_2_mat.at<double>(1,2) = -points_2[i].x;
    //        points_2_mat.at<double>(2,0) = -points_2[i].y; points_2_mat.at<double>(2,1) = points_2[i].x; points_2_mat.at<double>(2,2) = 0;

    //        cv::Mat A1, A2 = cv::Mat::zeros(3,4, CV_64F);
    //        cv::Mat A;

    //        A1 = points_1_mat * P1;
    //        A2 = points_2_mat * P2;

    //        //std::cout << "A1 " << A1 << std::endl;
    //        //std::cout << "A2 " << A2 << std::endl;

    //        A.push_back(A1);
    //        A.push_back(A2);

    //        //std::cout << "A "  << A << std::endl;
    //        cv::SVD svd(A, cv::SVD::MODIFY_A);

    //        //std::cout << "vt " << svd.vt << std::endl;
    //        points3d_new_method.at<float>(i,0) = svd.vt.at<float>(0,3)/svd.vt.at<float>(3,3);
    //        points3d_new_method.at<float>(i,1) = svd.vt.at<float>(1,3)/svd.vt.at<float>(3,3);
    //        points3d_new_method.at<float>(i,2) = svd.vt.at<float>(2,3)/svd.vt.at<float>(3,3);

    //    }

    //    std::cout << "points 3D new method " << points3d_new_method << std::endl;
    //cv::convertPointsFromHomogeneous(new_points4D, points3D);

}

//cv::Mat_<double> IterativeLinearLSTriangulation(cv::Point3d u,    //homogenous image point (u,v,1)
//                                                cv::Matx34d P,          //camera 1 matrix
//                                                cv::Point3d u1,         //homogenous image point in 2nd camera
//                                                cv::Matx34d P1          //camera 2 matrix
//                                                )
//{
//    double wi = 1, wi1 = 1;
//    Mat_<double> X(4,1);
//    for (int i=0; i<10; i++) { //Hartley suggests 10 iterations at most
//        Mat_<double> X_ = LinearLSTriangulation(u,P,u1,P1);
//        X(0) = X_(0); X(1) = X_(1); X(2) = X_(2); X_(3) = 1.0;

//        //recalculate weights
//        double p2x = Mat_<double>(Mat_<double>(P).row(2)*X)(0);
//        double p2x1 = Mat_<double>(Mat_<double>(P1).row(2)*X)(0);

//        //breaking point
//        if(fabsf(wi - p2x) <= EPSILON && fabsf(wi1 - p2x1) <= EPSILON) break;

//        wi = p2x;
//        wi1 = p2x1;

//        //reweight equations and solve
//        Matx43d A((u.x*P(2,0)-P(0,0))/wi,       (u.x*P(2,1)-P(0,1))/wi,         (u.x*P(2,2)-P(0,2))/wi,
//                  (u.y*P(2,0)-P(1,0))/wi,       (u.y*P(2,1)-P(1,1))/wi,         (u.y*P(2,2)-P(1,2))/wi,
//                  (u1.x*P1(2,0)-P1(0,0))/wi1,   (u1.x*P1(2,1)-P1(0,1))/wi1,     (u1.x*P1(2,2)-P1(0,2))/wi1,
//                  (u1.y*P1(2,0)-P1(1,0))/wi1,   (u1.y*P1(2,1)-P1(1,1))/wi1,     (u1.y*P1(2,2)-P1(1,2))/wi1
//                  );
//        Mat_<double> B = (Mat_<double>(4,1) <<    -(u.x*P(2,3)    -P(0,3))/wi,
//                          -(u.y*P(2,3)  -P(1,3))/wi,
//                          -(u1.x*P1(2,3)    -P1(0,3))/wi1,
//                          -(u1.y*P1(2,3)    -P1(1,3))/wi1
//                          );

//        solve(A,B,X_,DECOMP_SVD);
//        X(0) = X_(0); X(1) = X_(1); X(2) = X_(2); X_(3) = 1.0;
//    }
//    return X;
//}

void rgb_camera_pose_extraction::correspondences2D3D(cv::Mat points3D, std::vector<cv::DMatch> third_matches,
                                                     std::vector<cv::KeyPoint> keypoints_2, std::vector<cv::KeyPoint> keypoints_3,
                                                     std::vector<cv::Point2f> points_2, std::vector<cv::Point2f>& new_points_3, cv::Mat& new_points3D)
{
    new_points3D = cv::Mat::zeros(0,3, CV_32F);
    std::vector<cv::Point2f> new_points_2, points_3;
    for(int i = 0; i < third_matches.size(); ++i)
    {
        new_points_2.push_back(keypoints_2[third_matches[i].queryIdx].pt);
        points_3.push_back(keypoints_3[third_matches[i].trainIdx].pt);
        //std::cout << "third_matches queryIdx " << third_matches[i].queryIdx << std::endl;
        //std::cout << "third matches trainIdx " << third_matches[i].trainIdx << std::endl;
        //std::cout << "distance " << third_matches[i].distance << std::endl;
    }

    //    for(int i =0; i < points_3.size(); ++i)
    //        std::cout << "points 3 " << points_3[i] << std::endl;

    new_points_3.clear();
    for (int i=0; i < new_points_2.size(); ++i)
    {
        for (int j=0; j < points_2.size(); ++j)
        {
            if(abs(new_points_2[i].x == points_2[j].x) && abs(new_points_2[i].y == points_2[j].y))
            {
                //if(points3D.at<float>(j,2) > 0)
                {
                    new_points3D.push_back(points3D.row(j));
                    std::cout << "matched points of image 2 in two matches " << new_points_2[i] << std::endl;
                    new_points_3.push_back(points_3[i]);
                }
            }
        }
    }

//    for (int i = 0; i < new_points_3.size(); ++i)
//        std::cout << "matched points of the third image with the 2d points of second image " << new_points_3[i] << std::endl;


//    std::cout << "point3D " << points3D << std::endl;
//    for(int i = 0; i < new_points3D.rows; ++i)
//        std::cout << " matched point3D " << new_points3D.row(i) << std::endl;

//    std::cout << "points 2 size " << points_2.size() << " " << "points3D " << points3D.size() << std::endl;
//    std::cout << "new points_2 size " <<  new_points_2.size() << " " << "points_3 size " << new_points_3.size() << std::endl;
    //std::cout << "new points 2 " << new_points_2 << std::endl;
    //std::cout << "points_2 " << points_2 << std::endl;
}

void rgb_camera_pose_extraction::sovlePnPRansac(std::vector<cv::Point2f> points_3, cv::Mat points3D, cv::Mat K, cv::Mat dist_coeff , cv::Mat &R3, cv::Mat &T3)
{
    cv::Mat dis_coeff;
    dis_coeff = cv::Mat::zeros(4, 1, CV_64F);

    cv::solvePnPRansac(points3D, points_3, K, dis_coeff, R3, T3, true, CV_ITERATIVE);

    std::cout << "rotation R3 " << R3 << std::endl;
    std::cout << "translation t3" << T3 << std::endl;

}














