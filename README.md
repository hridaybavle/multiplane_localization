# README #

### What is this repository for? ###

This is a package for localization using planes extracted by point clouds.
Version 1 where only the altitude is estimated. 

### Setup ###

The basic dependencies of this packages are ROS and PCL. 
You will also need the EKF_SLAM library in the same ros workspace for compiling this code. 

Link: https://bitbucket.org/hridaybavle/ekf_slam

### Launching the node ###
Download the rosbag in the link and perform the following steps. 
Rosbag: https://www.dropbox.com/s/yrjbrjys52qq0b5/rosbag_w_50deg_res_lab_4.bag?dl=0.

1. rosbag play rosbag_w_50deg_res_lab_4.bag --clock
2. roslaunch multiplane_localization multiplane_localization.launch

The poincloud topic can be changed in the launch file, for this rosbag it should be /camera/depth/points. 
The altitude is published altitude can also be changed in the launch file. 
The poincloud can be viewed in Rviz.